﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImComing.Class
{
    public class Message
    {
        public String nom { get; set; }
        public String prenom { get; set; }
        public String tel { get; set; }
        public Position position { get; set; }
    }
}
