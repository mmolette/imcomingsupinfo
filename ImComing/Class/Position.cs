﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.Devices.Geolocation;

namespace ImComing.Class
{
    public class Position
    {
        public String adresse { get; set; }
        public String ville { get; set; }
        public String cp { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public DateTime date { get; set; }
    }
}
