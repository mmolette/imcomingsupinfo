﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ImComing.Class;
using Microsoft.Phone.Tasks;
using System.Collections.ObjectModel;

namespace ImComing.Pages
{
    public partial class DetailPosition : PhoneApplicationPage
    {
 
        public DetailPosition()
        {
            InitializeComponent();
         
        }

        private void bt_Delete_Click(object sender, RoutedEventArgs e)
        {
            
            App.posCollections.Remove((Position)PhoneApplicationService.Current.State["Position"]);
            IsoStoreHelper.SaveList<Position>("positionsFolder", "positions", App.posCollections);
            NavigationService.Navigate(new Uri("/Pages/MainPage.xaml", UriKind.Relative));
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Position currentPos = (Position)PhoneApplicationService.Current.State["Position"];
            currentPosition.DataContext = currentPos;
            base.OnNavigatedTo(e);
        }

        private void bt_DeleteAll_Click(object sender, RoutedEventArgs e)
        {
            App.posCollections.Clear();
            IsoStoreHelper.SaveList<Position>("positionsFolder", "positions", App.posCollections);
            NavigationService.Navigate(new Uri("/Pages/MainPage.xaml", UriKind.Relative));
        }



    }
}