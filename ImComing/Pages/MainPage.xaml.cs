﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ImComing.Resources;
using ImComing.Class;
using System.Collections.ObjectModel;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Maps.Services;
using System.Device.Location;
using System.Text;
using Microsoft.Phone.Tasks;

namespace ImComing
{
    public partial class MainPage : PhoneApplicationPage
    {

        private Geolocator geolocator;
        private Position myPosition;
        private ObservableCollection<Position> positions;

        // Constructeur
        public MainPage()
        {
            InitializeComponent();
            positions = new ObservableCollection<Position>();
            myPosition = new Position();
            

            positions = App.posCollections;

            //Message mess = new Message { nom = "test", prenom = "test" };
            //positions.Add(mess);
            PositionsList.ItemsSource = positions;       
        }
        
        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/Pages/NewDestination.xaml", UriKind.Relative));
            SmsComposeTask smsComposeTask = new SmsComposeTask();

            smsComposeTask.To = "";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Je suis ici :");
            sb.AppendLine(myPosition.adresse);
            sb.AppendLine(myPosition.cp);
            sb.AppendLine(myPosition.ville);
            smsComposeTask.Body = sb.ToString();

            smsComposeTask.Show();
        }

        private void PositionsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(PositionsList.SelectedItem == null){
                return;
            }else{
                Position selectedPosition = (Position) PositionsList.SelectedItem;
                PhoneApplicationService.Current.State["Position"] = selectedPosition;
                PositionsList.SelectedItem = null;
                NavigationService.Navigate(new Uri("/Pages/DetailPosition.xaml", UriKind.Relative));
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            geolocator = new Geolocator { DesiredAccuracy = PositionAccuracy.High, MovementThreshold = 20 };
            geolocator.StatusChanged += geolocator_StatusChanged;
            geolocator.PositionChanged += geolocator_PositionChanged;

            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            geolocator.PositionChanged -= geolocator_PositionChanged;
            geolocator.StatusChanged -= geolocator_StatusChanged;
            geolocator = null;

            base.OnNavigatedFrom(e);
        }

        private void geolocator_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            string status = "";

            switch (args.Status)
            {
                case PositionStatus.Disabled:
                    status = "Le service de localisation est désactivé dans les paramètres";
                    break;
                case PositionStatus.Initializing:
                    status = "En cours d'initialisation";
                    break;
                case PositionStatus.Ready:
                    status = "Service de localisation prêt";
                    break;
                case PositionStatus.NotAvailable:
                case PositionStatus.NotInitialized:
                case PositionStatus.NoData:
                    status = "Service de localisation inaccesible";
                    break;
            }

            Dispatcher.BeginInvoke(() =>
            {
                if (status == "Service de localisation inaccesible")
                {
                    MessageBox.Show(status);
                }
            });
        }

        private void geolocator_PositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            Dispatcher.BeginInvoke(() =>
            {
                this.myPosition.latitude = args.Position.Coordinate.Latitude;
                this.myPosition.longitude = args.Position.Coordinate.Longitude;
                Maps_ReverseGeoCoding();
            });
            
        }

        private void Maps_ReverseGeoCoding()
        {
            ReverseGeocodeQuery query = new ReverseGeocodeQuery()
            {
                GeoCoordinate = new GeoCoordinate(this.myPosition.latitude, this.myPosition.longitude)
            };
            query.QueryCompleted += query_QueryCompleted;
            query.QueryAsync();
        }

        void query_QueryCompleted(object sender, QueryCompletedEventArgs<IList<MapLocation>> e)
        {
 
            foreach (var item in e.Result)
            {
                myPosition.adresse = item.Information.Address.HouseNumber + " " + item.Information.Address.Street;
                myPosition.cp = item.Information.Address.PostalCode;
                myPosition.ville = item.Information.Address.City;
                myPosition.date = DateTime.Now;
            }

            positions.Add(myPosition);
            IsoStoreHelper.SaveList<Position>("positionsFolder", "positions", positions);

            ShellTile tuileParDefaut = ShellTile.ActiveTiles.First();

            if (tuileParDefaut != null)
            {
                FlipTileData flipTileData = new FlipTileData
                {
                    Title = "I'm Here",
                    BackTitle = myPosition.adresse,
                    BackContent = myPosition.date.ToString(),
                };

                tuileParDefaut.Update(flipTileData);
            }
        }
    }
}